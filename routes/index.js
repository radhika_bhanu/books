var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
require('../models/People'); // require the models before the connect
require('../models/Books');
var Person = mongoose.model('Person');
var Book = mongoose.model('Book');

/* GET home page. */
router.get('/people', function(req, res, next) {
  // Person.find(function(err, people){
  // 	 if(err){ return next(err); }
  // 	 res.json(people);
  // });
  Person.find({}).populate('books').exec(function(err, people) {
    if(err){ return next(err); }
    res.json(people);
    console.log('populated each person books');
  });

});

router.get('/books/:person', function(req, res) {
   req.person.populate('books', function(err, person) {
    if (err) { return console.log(err); }
    res.json(person);
  });
});

router.param('person', function(req, res, next, id) {
  var query = Person.findById(id);

  query.exec(function (err, person){
    if (err) { return next(err); }
    if (!person) { return next(new Error('can\'t find person!')); }

    req.person = person;
    return next();
  });
});

router.get('/', function(req, res){
  res.render('index');
});
module.exports = router;
