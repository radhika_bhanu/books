var parseString = require('xml2js').parseString;
var fs = require('fs');
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var express = require('express');
var app = express();
var _ = require('underscore');
var mongoose = require('mongoose');
require('./models/People');
require('./models/Books'); // require the models before the connect
var Person = mongoose.model('Person');
var Book = mongoose.model('Book');
mongoose.connect('mongodb://localhost/books');

var db = mongoose.connection;


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var bookData = {
	"The Player of Games (Culture, #2)" : {// no india
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1388426980i/12012._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1RvkRog",
			flipkart :"http://www.flipkart.com/iain-m-banks-consider-phlebas-player-games-use-weapons/p/itmdex4urfg4dubp?pid=9780356502090&ref=L%3A83036695816586115&srno=p_6&query=iain+banks&otracker=from-search"
		}
	},
	"The Ends of Power" : { // not india
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1411354727i/401465._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1Lnw6Kj",
			flipkart : ""
		}
	},
	"The Innovator's Dilemma: The Revolutionary Book That Will Change the Way You Do Business" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1347289672i/9848159._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1RvjzcY",
			flipkart :"http://www.flipkart.com/innovator-s-dilemma-revolutionary-national-bestseller-changed-way-do-business-english/p/itme3u2kn7zgqbtf?pid=9780066620695&ref=L%3A4875851781565990262&srno=p_7&query=The+Innovator%27s+Dilemma%3A+The+Revolutionary+Book+That+Will+Change+the+Way+You+Do+Business&otracker=from-search"
		}
	},
	"Should We Eat Meat?: Evolution and Consequences of Modern Carnivory" : {
		affLink : {
			amazon : "http://www.amazon.com/gp/product/1118278720/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=1118278720&linkCode=as2&tag=whatdotheyrea-20&linkId=C5IN2TYKIJC2MHWD",
			flipkart : "http://www.flipkart.com/should-eat-meat-evolution-consequences-modern-carnivory-english/p/itmdhz6ys5kfgakj?pid=9781118278727&ref=L%3A-5950077028056743935&srno=p_2&query=Should+We+Eat+Meat%3F%3A+Evolution+and+Consequences+of+Modern+Carnivory&otracker=from-search"
		}
	},
	"The Magic of Reality: How We Know What's Really True" : {
		affLink : {
			amazon : "http://amzn.to/1QZBdom",
			flipkart : "http://www.flipkart.com/magic-reality-know-what-s-really-true-english/p/itmdyhs4bgxyu4ts?pid=9781442341760&ref=L%3A8669594809565267249&srno=p_2&query=The+Magic+of+Reality%3A+How+We+Know+What%27s+Really+True&otracker=from-search"
		}
	},
	"On Immunity: An Inoculation" : {
		affLink : {
			amazon : "http://amzn.to/1PjHrB0",
			flipkart : "http://www.flipkart.com/on-immunity-an-inoculation/p/itme8xdfs7zz7qxp?pid=9781555976897&ref=L%3A-8124637173867263314&srno=p_1&query=On+Immunity%3A+An+Inoculation&otracker=from-search"
		}
	},
	"Hyperbole and a Half: Unfortunate Situations, Flawed Coping Mechanisms, Mayhem, and Other Things That Happened" : {
		affLink : {
			amazon : "http://amzn.to/1OnkLiL",
			flipkart : "http://www.flipkart.com/hyperbole-half-unfortunate-situations-flawed-coping-mechanisms-mayhem-other-things-happened-english/p/itme4rz649kqyy47?pid=9780224095372&ref=L%3A8895405855863685221&srno=p_1&query=Hyperbole+and+a+Half%3A+Unfortunate+Situations%2C+Flawed+Coping+Mechanisms%2C+Mayhem%2C+and+Other+Things+That+Happened&otracker=from-search"
		}
	},
	"How to Lie with Statistics" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1384474531i/18805905._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1PjHwET",
			flipkart : "http://www.flipkart.com/lie-statistics-english-reissue-4th/p/itme9hf5thamyjxe?pid=9780393310726&ref=L%3A8242699976335772473&srno=p_1&query=How+to+Lie+with+Statistics&otracker=from-search"
		}
	},
	"Only the Paranoid Survive " : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1396021991i/66864._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1Lntdt2",
			flipkart : "http://www.flipkart.com/only-paranoid-survive-english/p/itme8ussu38ghvwm?pid=9781861975133&ref=L%3A887925229179195052&srno=p_1&query=Only+the+Paranoid+Survive+&otracker=from-search"
		}
	},
	"xkcd: volume 0" : {
		affLink : {
			amazon : "http://amzn.to/1RvjFRZ",
			flipkart : "http://www.flipkart.com/xkcd-english/p/itmczzjxtsxgpqyn?pid=9780615314464&icmpid=reco_pp_same_book_book_1&ppid=9785458921190"
		}
	},
	"The Black Jacobins: Toussaint L'Ouverture and the San Domingo Revolution" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1349074537i/113165._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1PjHLzG",
			flipkart : "http://www.flipkart.com/black-jacobins-toussaint-l-ouverture-san-domingo-revolution-english/p/itme4bufwzkjp3wy?pid=9780679724674&ref=L%3A-7153230391165694848&srno=p_2&query=The+Black+Jacobins%3A+Toussaint+L%27Ouverture+and+the+San+Domingo+Revolution&otracker=from-search"
		}
	},
	"The Power Of Focus" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1348442147i/11056574._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1LntMmy",
			flipkart : "http://www.flipkart.com/power-focus-english/p/itmdyspc7u45gnst?pid=9780091948221&ref=L%3A-6770229431573985492&srno=p_1&query=the+power+of+focus&otracker=from-search"
		}
	},
	"Howard Hughes: The Untold Story" : {
		affLink : {
			amazon : "http://amzn.to/1OnlobP",
			flipkart : "http://www.flipkart.com/howard-hughes-untold-story-english/p/itme9hfs7qtsungt?pid=9780306813924&ref=L%3A4407346068460158994&srno=p_1&query=Howard+Hughes%3A+The+Untold+Story&otracker=from-search"
		}
	},
	"Technological Revolutions and Financial Capital: The Dynamics of Bubbles and Golden Ages" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1302073297i/9894874._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1LntZ9o",
			flipkart : "http://www.flipkart.com/technological-revolutions-financial-capital-dynamics-bubbles-golden-ages-english/p/itmdyh8agb6gcfmw?pid=9781843763314&ref=L%3A-7219659477854697491&srno=p_1&query=Technological+Revolutions+and+Financial+Capital%3A+The+Dynamics+of+Bubbles+and+Golden+Ages&otracker=from-search"
		}
	},
	"What If?: Serious Scientific Answers to Absurd Hypothetical Questions" : {
		affLink : {
			amazon : "http://amzn.to/1Rvk7zA",
			flipkart : "http://www.flipkart.com/if-serious-scientific-answers-absurd-hypothetical-questions/p/itme9y5ajxkhpdje?pid=9781848549586&ref=L%3A-5176779643976726767&srno=p_1&query=What+If%3F%3A+Serious+Scientific+Answers+to+Absurd+Hypothetical+Questions&otracker=from-search"
		}
	},
	"Einstein: His Life and Universe" : {
		affLink : {
			amazon : "http://amzn.to/1PjI2ml",
			flipkart : "http://www.flipkart.com/einstein-his-life-universe/p/itme8nndf5kzntgp?pid=DGBDFBZJHWCXFHJW&ref=L%3A-2281524841627433653&srno=p_1&query=Einstein%3A+His+Life+and+Universe&otracker=from-search"
		}
	},
	"My Years with General Motors" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1348442995i/16042934._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1Rvko5J",
			flipkart : "http://www.flipkart.com/my-years-general-motors-english/p/itmczz46jzjtd75g?pid=9780385042352&ref=L%3A1139218216003518572&srno=p_1&query=My+Years+with+General+Motors&otracker=from-search"
		}
	},
	"The Hard Thing About Hard Things: Building a Business When There Are No Easy Answers" : {
		affLink : {
			amazon : "http://amzn.to/1Rvkw4X",
			flipkart : "http://www.flipkart.com/hard-thing-things-building-business-there-no-easy-answers/p/itme37hpzryjzfbb?pid=DGBDTJR3MXT4A4GC&ref=L%3A-8645542411795805676&srno=p_1&query=The+Hard+Thing+About+Hard+Things%3A+Building+a+Business+When+There+Are+No+Easy+Answers&otracker=from-search"
		}
	},
	"Benjamin Franklin: An American Life" : {
		affLink : {
			amazon : "http://amzn.to/1RvkB8W",
			flipkart : "http://www.flipkart.com/benjamin-franklin-american-life-english/p/itme8n859x3jbxcc?pid=9780684807614&ref=L%3A-8343035215280847503&srno=p_1&query=Benjamin+Franklin%3A+An+American+Life&otracker=from-search"
		}
	},
	"Ignition (Ignition Trilogy, #1)" : {
		affLink : {
			amazon : "http://amzn.to/1Lnv7tu"
		}
	},
	"Catherine the Great: Portrait of a Woman" : {
		affLink : {
			amazon : "http://amzn.to/1Lnvd4t",
			flipkart : "http://www.flipkart.com/catherine-great-portrait-woman-english/p/itme8n84tayuatgg?pid=9780345408778&ref=L%3A4731827918300856713&srno=p_1&query=Catherine+the+Great%3A+Portrait+of+a+Woman&otracker=from-search"
		}
	},
	"The Better Angels of Our Nature: Why Violence Has Declined" : {
		affLink : {
			amazon : "http://amzn.to/1Onnw3l",
			flipkart : "http://www.flipkart.com/better-angels-our-nature-violence-has-declined-english/p/itme576kdtwgwnzu?pid=9780143122012&ref=L%3A4002474946583675222&srno=p_1&query=The+Better+Angels+of+Our+Nature%3A+Why+Violence+Has+Declined&otracker=from-search"
		}
	},
	"Sapiens: A Brief History of Humankind" : {
		affLink : {
			amazon : "http://amzn.to/1OnnCrN",
			flipkart : "http://www.flipkart.com/sapiens-brief-history-humankind/p/itme9y3nh4f4aq8b?pid=9781846558245&ref=L%3A-1076715148720913953&srno=p_1&query=Sapiens%3A+A+Brief+History+of+Humankind&otracker=from-search"
		}
	},
	"Creativity, Inc.: Overcoming the Unseen Forces That Stand in the Way of True Inspiration" : {
		affLink : {
			amazon : "http://amzn.to/1OnnGYt",
			flipkart : "http://www.flipkart.com/creativity-inc-overcoming-unseen-forces-stand-way-true-inspiration-english/p/itmdzar5s2fhmv3e?pid=9780593070109&ref=L%3A-7396808933550698985&srno=p_1&query=Creativity%2C+Inc.%3A+Overcoming+the+Unseen+Forces+That+Stand+in+the+Way+of+True+Inspiration&otracker=from-search"
		}
	},
	"Muqaddimah Aqidah Muslimin" : {
		affLink : {
			amazon : "",
			flipkart : "http://www.flipkart.com/muqaddimah-introduction-history-english/p/itme2wknttfx4ytv?pid=9780691166285&ref=L%3A5760362653088366881&srno=p_1&query=muqqadimah&otracker=from-search"
		}
	}, 
	"The New Jim Crow: Mass Incarceration in the Age of Colorblindness" : {
		affLink : {
			amazon : "http://amzn.to/1LnvUuF",
			flipkart : "http://www.flipkart.com/new-jim-crow-english/p/itmdyk7u6nwfrsne?pid=9781595581037&ref=L%3A328462671577498155&srno=p_1&query=The+New+Jim+Crow&otracker=from-search	"
		}
	},
	"A Short Guide to a Happy Life" : {
		affLink : {
			amazon : "http://amzn.to/1JWyu9Q",
			flipkart : "http://www.flipkart.com/short-guide-happy-life-english/p/itmczz3skhvywjr3?pid=9780375504617&ref=L%3A607815422786030138&srno=p_1&query=a+short+guide+to+a+happy+life&otracker=from-search"
		}
	},
	"Bossypants" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1328015785i/11021913._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1WQQ74a",
			flipkart : "http://www.flipkart.com/bossypants-english/p/itme8n7hhwkzjwrd?pid=9780751547832&ref=L%3A8332078378241837390&srno=p_1&query=Bossypants&otracker=from-search"
		}
	},
	"Conscious Business: How to Build Value Through Values" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1438488368i/18753268._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1JWyN4t",
			flipkart : "http://www.flipkart.com/conscious-business-build-value-through-values-english-large-print-16-pt/p/itmd3fegdyzyryg9?pid=9781427098184&ref=L%3A4766331584385927849&srno=p_1&query=Conscious+Business%3A+How+to+Build+Value+Through+Values&otracker=from-search",
		}
	},
	"Home Game: An Accidental Guide to Fatherhood" : {
		affLink : {
			amazon : "http://amzn.to/1JWAkr6",
			flipkart : "http://www.flipkart.com/home-game-accidental-guide-fatherhood/p/itme56ujgbc6gs2z?pid=DGBDJ8X44ZUJXHHJ&ref=L%3A-6699053601784574338&srno=p_1&query=Home+Game%3A+An+Accidental+Guide+to+Fatherhood&otracker=from-search"
		}
	},
	"Now, Discover Your Strengths" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1388728778i/20423717._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1QdnvOx",
			flipkart : "http://www.flipkart.com/now-discover-your-strengths-english/p/itmdzutpm88n7ms4?pid=9781416502654&ref=L%3A-7082428381350180753&srno=p_1&query=Now%2C+Discover+Your+Strengths&otracker=from-search"
		}
	},
	"Queen of Fashion: What Marie Antoinette Wore to the Revolution" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1317065068i/450381._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1Rxs3QX",
			flipkart :"http://www.flipkart.com/queen-fashion-marie-antoinette-wore-revolution-english/p/itmczzyepgeaabsh?pid=9780805079494&ref=L%3A4490713437890298892&srno=p_1&query=Queen+of+Fashion%3A+What+Marie+Antoinette+Wore+to+the+Revolution&otracker=from-search"
		}
	},
	"The Lean Startup: Book Summary - Eric Ries - How Constant Innovation Creates Radically Successful Businesses" : {
		affLink : {
			amazon : "http://amzn.to/1WQRXC1",
			flipkart : "http://www.flipkart.com/lean-startup-constant-innovation-creates-radically-successful-businesses-english/p/itme32gcgchwn7jv?pid=9780670921607&ref=L%3A9064326520212704943&srno=p_1&query=lean+startup&otracker=from-search"
		}
	},
	"Stalingrad: The Fateful Siege, 1942-1943" : {
		pic: "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1387706486i/309376._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1G4lkfT",
			flipkart : "http://www.flipkart.com/stalingrad-english/p/itmdx2maumavr3xv?pid=9780141032405&ref=L%3A7261891395497574995&srno=p_2&query=stalingrad&otracker=from-search"
		}
	},
	"The Dice Man" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1340739642i/828607._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1G4luUE",
			flipkart : "http://www.flipkart.com/dice-man-english/p/itmczymfnmrv4mza?pid=9780006513902&ref=L%3A-5174072962776740914&srno=p_2&query=The+Dice+Man&otracker=from-search"
		}
	},
	"Swallows and Amazons" : {
		affLink : {
			amazon : "http://amzn.to/1jkIdkQ"
		}
	},
	"Wild Swans: Three Daughters of China" : {
		pic:"https://d.gr-assets.com/books/1440643710l/1848.jpg",
		affLink : {
			amazon : "http://amzn.to/1jkIrZb",
			flipkart : "http://www.flipkart.com/wild-swans-english/p/itmdz6sg7rqjf6js?pid=9780007463404&icmpid=reco_pp_same_book_book_1&ppid=9780743246989"
		}
	},
	"Tales of the Unexpected" : {
		affLink : {
			amazon  : "http://amzn.to/1jh7oFD",
			flipkart : "http://www.flipkart.com/tales-unexpected-english/p/itmdzvszhwwdvhrk?pid=9780679729891&ref=L%3A1639914169593775932&srno=p_1&query=Tales+of+the+Unexpected&otracker=from-search"
		}
	},
	"The Quiet American" : {
		pic : "https://d.gr-assets.com/books/1388187216l/3698.jpg",
		affLink : {
			amazon : "http://amzn.to/1G4mPLb",
			flipkart : "http://www.flipkart.com/quiet-american-english/p/itme8mwbzm3znhnv?pid=9780099478393&ref=L%3A5975058306813543979&srno=p_1&query=The+Quiet+American&otracker=from-search"
		}
	},
	"The Weather Makers: How Man Is Changing the Climate and What It Means for Life on Earth" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1441169556i/534491._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1jkJCb6",
			flipkart : "http://www.flipkart.com/weather-makers-man-changing-climate-means-life-earth-english/p/itmdyfpfhbz5hvam?pid=9781419390685&ref=L%3A-1594146319451993012&srno=p_2&query=The+Weather+Makers%3A+How+Man+Is+Changing+the+Climate+and+What+It+Means+for+Life+on+Earth&otracker=from-search"
		}
	},
	"Where the Wild Things Are" : {
		pic : "https://d.gr-assets.com/books/1384434560l/19543.jpg",
		affLink : {
			amazon : "http://amzn.to/1P6ehGr",
			flipkart : "http://www.flipkart.com/wild-things-english/p/itme8nc8ah5fuzyu?pid=9780099408390&ref=L%3A3627707292887238382&srno=p_1&query=Where+the+Wild+Things+Are&otracker=from-search"
		}
	},
	"The Illustrated Long Walk to Freedom: The Autobiography of Nelson Mandela" : {
		affLink : {
			amazon : "http://amzn.to/1jh865G",
			flipkart : "http://www.flipkart.com/long-walk-freedom-english/p/itme9fy7fdtzdshp?pid=9780349106533&ref=L%3A2967493491956926601&srno=p_2&query=The+Illustrated+Long+Walk+to+Freedom%3A+The+Autobiography+of+Nelson+Mandela&otracker=from-search"
		}
	},
	"A Brief History of Time" : {
		affLink : {
			amazon : "http://amzn.to/1jkMlBo",
			flipkart : "http://www.flipkart.com/stephen-hawking-brief-history-time-grand-design-set-2-books-english/p/itmefjeggf9yzxfy?pid=9780857502988&ref=L%3A-2295640542532124347&srno=p_8&query=stephen+hawking&otracker=from-search"
		}
	},
	"Black Skin, White Masks" : {
		pic : "https://d.gr-assets.com/books/1386923176s/274392.jpg",
		affLink : {
			amazon : "http://amzn.to/1GCshje",
			flipkart : "http://www.flipkart.com/black-skin-white-masks-english/p/itmdyy982ygtv9yv?pid=9780802143006&ref=L%3A-6826112490271914772&srno=p_1&query=Black+Skin%2C+White+Masks&otracker=from-search"
		}
	},
	"The Discovery of India" : {
		affLink : {
			amazon : "http://amzn.to/1GCskvl",
			flipkart : "http://www.flipkart.com/discovery-india-english/p/itmdyt4fk6uccczf?pid=9780143031031&ref=L%3A979559828029128987&srno=p_1&query=The+Discovery+of+India&otracker=from-search"
		}
	},
	"What Money Can't Buy: The Moral Limits of Markets" : {
		affLink : {
			amazon : "http://amzn.to/1jWGPFH",
			flipkart : "http://www.flipkart.com/money-can-t-buy-moral-limits-markets-english/p/itme9zdb44qmpgah?pid=9781846144721&ref=L%3A-4622264055218944690&srno=p_6&query=What+Money+Can%27t+Buy%3A+The+Moral+Limits+of+Markets&otracker=from-search"
		}
	},
	"The Story of Civilization (11 Volume Set)" : {
		affLink : {
			amazon : "http://amzn.to/1jWGWRz",
			flipkart : "http://www.flipkart.com/story-civilization-11-volume-set-english/p/itmdyj2s9qpf6czn?pid=9781567310238&ref=L%3A6500056385305919900&srno=p_3&query=The+Story+Of+Civilization+%2811+Volume+Set%29&otracker=from-search"
		}
	},
	"The Republic" : {
		pic : "http://d.gr-assets.com/books/1386925655l/30289.jpg",
		affLink : {
			amazon : "http://amzn.to/1jWH2IN",
			flipkart : "http://www.flipkart.com/the-republic-english/p/itme8vuagrghtpns?pid=9789380816371&ref=L%3A6761836408076324155&srno=p_2&query=The+Republic&otracker=from-search"
		}
	},
	"Capital in the Twenty-First Century" : {
		pic : "http://d.gr-assets.com/books/1390111547l/18736925.jpg",
		affLink : {
			amazon : "http://amzn.to/1VLPd6u",
			flipkart : "http://www.flipkart.com/capital-twenty-first-century/p/itme9u7bvvdmrhsp?pid=9780674430006&ref=L%3A-6584280157800870213&srno=p_1&query=Capital+in+the+Twenty-First+Century&otracker=from-search"
		}
	},
	"The Thinker's Guide to the Art of Socratic Questioning" : {
		pic : "http://d.gr-assets.com/books/1400536122l/7276284.jpg",
		affLink : {
			amazon : "http://amzn.to/1jijoGO"

		}
	},
	"Winners Never Cheat: Even in Difficult Times" : {
		pic : "http://d.gr-assets.com/books/1348961035l/5449798.jpg",
		affLink : {
			amazon : "http://amzn.to/1GCsvab",
			flipkart : "http://www.flipkart.com/winners-never-cheat-even-difficult-times-english-1st/p/itmdyyur9fsevmmh?pid=9789332518933&ref=L%3A3653065671797721043&srno=p_1&query=Winners+Never+Cheat%3A+Even+in+Difficult+Times&otracker=from-search"
		}
	},
	"The Story of My Experiments With Truth" : {
		affLink : {
			amazon : "http://amzn.to/1GCszH0",
			flipkart : "http://www.flipkart.com/autobiography-story-my-experiments-truth-pb-english/p/itme9zndgvyxumae?pid=9788188951390&ref=L%3A4486953972907132019&srno=p_1&query=The+Story+of+My+Experiments+With+Truth&otracker=from-search"
		}
	},
	"The Protestant Ethic and the Spirit of Capitalism" : {
		pic : "http://d.gr-assets.com/books/1386921121l/74176.jpg",
		affLink : {
			amazon : "http://amzn.to/1VLPx5f",
			flipkart : "http://www.flipkart.com/protestant-ethic-spirit-capitalism-english/p/itme9kgd3ytqzvn5?pid=9781603866040&ref=L%3A93161843665553553&srno=p_2&query=The+Protestant+Ethic+and+the+Spirit+of+Capitalism&otracker=from-search"
		}
	},
	"Moby-Dick; or, The Whale" : {
		affLink : {
			amazon : "http://amzn.to/1VWjSU4",
			flipkart : "http://www.flipkart.com/moby-dick-or-whale-english/p/itme9cgahsmxfzhg?pid=9788175992771&ref=L%3A-4901365844961049508&srno=p_1&query=Moby-Dick%3B+or%2C+The+Whale&otracker=from-search"
		}
	},
	"Be Here Now" : {
		affLink : {
			amazon : "http://amzn.to/1Ldplha",
			flipkart : "http://www.flipkart.com/be-here-now/p/itme9ufjcc9929ps?pid=9780517543054&ref=L%3A-3838540731754658706&srno=p_1&query=Be+Here+Now&otracker=from-search"
		}
	},
	"Inside the Tornado: Strategies for Developing, Leveraging, and Surviving Hypergrowth Markets" : {
		pic : "http://ecx.images-amazon.com/images/I/514-WV%2BwQjL._SX331_BO1,204,203,200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1GhqqWm",
			flipkart : "http://www.flipkart.com/inside-tornado-strategies-developing-leveraging-surviving-hypergrowth-markets-english/p/itmdun7thz7pgj69?pid=9780062312778&ref=L%3A2604129907487753095&srno=p_1&query=Inside+the+Tornado%3A+Strategies+for+Developing%2C+Leveraging%2C+and+Surviving+Hypergrowth+Markets&otracker=from-search"
		}
	},
	"Atlas Shrugged" : {
		affLink : {
			amazon : "http://amzn.to/1GhqGEE",
			flipkart : "http://www.flipkart.com/atlas-shrugged-english/p/itmeyyufrtwvhnkr?pid=9780451191144&ref=L%3A8469164179621430869&srno=p_1&query=Atlas+Shrugged&otracker=from-search"
		}
	},
	"Zen Mind, Beginner's Mind: Informal Talks on Zen Meditation and Practice" : {
		affLink : {
			amazon : "http://amzn.to/1VWkAAL",
			flipkart : "http://www.flipkart.com/zen-mind-beginner-s-mind-informal-talks-meditation-practice-english/p/itmdyym4wqspugb7?pid=9780834800793&ref=L%3A-2347586896513643880&srno=p_1&query=Zen+Mind%2C+Beginner%27s+Mind%3A+Informal+Talks+on+Zen+Meditation+and+Practice&otracker=from-search"
		}
	},
	"King Lear" : {
		affLink : {
			amazon : "http://amzn.to/1GhqR2N",
			flipkart : "http://www.flipkart.com/king-lear-english/p/itme8qchejdkfsvx?pid=9781853260957&ref=L%3A6706998134590033282&srno=p_1&query=King+Lear&otracker=from-search"
		}
	},
	"Night (The Night Trilogy, #1)" : {
		affLink : {
			amazon : "http://amzn.to/1Ghz3QA",
			flipkart : "http://www.flipkart.com/night-elie-wiesel-english/p/itmd9mby8gntx5tg?pid=9789381753279&ref=L%3A-7423366148461998985&srno=p_1&query=night+elie&otracker=from-search"
		}
	},
	"The Story of Edgar Sawtelle" : {
		affLink : {
			amazon : "http://amzn.to/1LdD47I",
			flipkart : "http://www.flipkart.com/story-edgar-sawtelle/p/itmdyszyswgv7fdw?pid=DGBDGKXFFQPV4CH3&ref=L%3A9027337512538820104&srno=p_1&query=The+Story+of+Edgar+Sawtelle&otracker=from-search"
		}
	},
	"The Bluest Eye" : {
		pic : "http://ecx.images-amazon.com/images/I/41gSSNgpYJL._SX322_BO1,204,203,200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1GhzaeX",
			flipkart : "http://www.flipkart.com/bluest-eye-english/p/itmeycgy5xz238bd?pid=9780099759911&ref=L%3A-2414310470161054580&srno=p_1&query=The+Bluest+Eye&otracker=from-search"
		}
	},
	"The Poisonwood Bible" : {
		affLink : {
			flipkart : "http://www.flipkart.com/the-poisonwood-bible/p/itmdxzh4qpmmktyf?pid=9780571298846&ref=L%3A3528824160316509611&srno=p_3&query=The+Poisonwood+Bible&otracker=from-search",
			amazon : "http://amzn.to/1GhzkDd"
		}
	},
	"East of Eden" : {
		affLink : {
			flipkart : "http://www.flipkart.com/east-eden-english/p/itme7vw9afzsj46d?pid=9780141185071&ref=L%3A-8967641069488877792&srno=p_1&query=East+of+Eden&otracker=from-search",
			amazon : "http://amzn.to/1LxhigI"
		}
	},
	"The Pillars of the Earth  (The Pillars of the Earth, #1)" : {
		pic : "http://ecx.images-amazon.com/images/I/51LjJnKxt0L._SX331_BO1,204,203,200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1LxhvQP",
			flipkart : "http://www.flipkart.com/pillars-earth-english/p/itme9qs7rwkzg4zr?pid=9780330450867&ref=L%3A-560927044827332189&srno=p_1&query=The+Pillars+of+the+Earth++%28The+Pillars+of+the+Earth%2C+%231%29&otracker=from-search"
		}
	},
	"A Fine Balance"  : {
		pic : "http://ecx.images-amazon.com/images/I/4136gEDpxeL._SX317_BO1,204,203,200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1LxhNaq",
			flipkart :"http://www.flipkart.com/fine-balance-b-english/p/itmduz8vsjrwyc5p?pid=9780571230587&ref=L%3A943337394164679647&srno=p_1&query=A+Fine+Balance&otracker=from-search"
		}
	},
	"A New Earth: Awakening to Your Life's Purpose" : {
		pic : "http://ecx.images-amazon.com/images/I/41XhFsjCNML._AC_UL480_SR318,480_.jpg",
		affLink : {
			amazon : "http://amzn.to/1GhApL7",
			flipkart : "http://www.flipkart.com/new-earth-awakening-your-life-s-purpose-english/p/itme4jbuueh9zzcd?pid=9781594152498&ref=L%3A-1454946624221395013&srno=p_1&query=A+New+Earth%3A+Awakening+to+Your+Life%27s+Purpose&otracker=from-search"
		}
	},
	"The Known World" : {
		pic : "http://ecx.images-amazon.com/images/I/31AuiyDUzwL._BO1,204,203,200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1VWzcQL",
			flipkart : "http://www.flipkart.com/known-world-english/p/itme4xb8xvhhye8j?pid=9780060557553&ref=L%3A744275519235572359&srno=p_1&query=the+known+world&otracker=from-search"
		}
	}
 
};
app.get('/', function(request,response){
	//var bookTitles = ['Hyperbole and a Half: Unfortunate Situations, Flawed Coping Mechanisms, Mayhem, and Other Things That Happened', 'The Magic of Reality: How We Know What\'s Really True '];

var data = {
	// "Dr. APJ Abdul Kalam" : ['Light from Many Lamps','Thirukural','Man the Unknown','Empires of the Mind: Lessons to Lead and Succeed In a Knowledge-Based world','Everyday Greatness','Medicine and Compassion: A Tibetan Lama\'s Guidance for Caregivers'],
	"Sheryl Sandberg" : ['A Short Guide to a Happy Life', 'Bossypants', 'Conscious Business','Home Game','Now, Discover Your Strengths', 'Queen of Fashion'],
	"Mark Zuckerberg" : ['The Muqaddimah', 'Sapiens','The New Jim Crow','The End of Power','Creativity, Inc.','The Better Angels of Our Nature','On Immunity','The Player of Games'],
	"Oprah Winfrey" : ['A New Earth','The Poisonwood Bible', 'Night','A Fine Balance','East of Eden','The Story of Edgar Sawtelle','The Pillars of the Earth','The Bluest Eye','The Known world'],
	"Bill Gates" : ['Hyperbole and a Half: Unfortunate Situations, Flawed Coping Mechanisms, Mayhem, and Other Things That Happened', 'The Magic of Reality: How We Know What\'s Really True','What If?: Serious Scientific Answers to Absurd Hypothetical Questions','XKCD: Volume 0','On Immunity: An Inoculation','How to Lie With Statistics','Should We Eat Meat? Evolution and Consequences of Modern Carnivory'],
	"Elon Musk" : ['Benjamin Franklin','Catherine the Great','Einstein','Howard Hughes','Ignition!'],
	"Ben Horowitz" : ['The Hard Thing About Hard Things','The Innovator\'s Dilemma', 'The Black Jacobins','Only the Paranoid Survive','Focus','My Years with General Motors','Technological Revolutions and Financial Capital: The Dynamics of Bubbles and Golden Ages'],
	"Richard Branson" : ['Stalingrad: The Fateful Siege', 'Wild Swans: Three Daughters of China', 'Swallows and Amazons','Dice Man', 'Where the Wild Things Are', 'Tales of the Unexpected','The Quiet American', 'The Weather Makers: How Man Is Changing the Climate and What It Means for Life on Earth','A Brief History of Time'],
	"NR Narayana Murthy" : ['Winners Never Cheat Even in Difficult Times','What Money Can\'t Buy: The Moral Limits of Markets','The Republic',' Black Skin, White Masks','The Thinker\'s Guide to The Art of Socratic Questioning','The Story of Civilization','Capital in the Twenty-First Century','The Discovery of India',' The Story of My Experiments with Truth','The Protestant Ethic and the Spirit of Capitalism'],
	"Steve Jobs" : ['King Lear','Moby Dick','Atlas Shrugged', 'The Innovator\'s Dilemma','Be Here Now','Only the Paranoid Survive', 'Inside the tornado','Zen Mind, Beginner\'s Mind']
},
	innerCount = 0,
	finalCount = 0,
	eachCount = {},
	generalData = {
		"Steve Jobs" : {
			description : "Co-founder, Apple",
			big_description: "Steven Jobs was an American businessman. He was best known as the co-founder, chairman, and CEO of Apple Inc.; CEO and largest shareholder of Pixar Animation Studios; member of The Walt Disney Company's board of directors following its acquisition of Pixar; and founder, chairman, and CEO of NeXT Inc.",
			location: "",
			company : "Apple",
			quote : "Innovation distinguishes between a leader and a follower."
		},
		"Dr. APJ Abdul Kalam" : {
			description : "11th President of India, Scientist",
			location : "Tamil Nadu, India",
			company : "",
			big_description : "APJ Abdul Kalam was the 11th President of India from 2002 to 2007. A career scientist turned reluctant politician, Kalam was born and raised in Rameswaram, Tamil Nadu, and studied physics and aerospace engineering. He spent the next four decades as a scientist and science administrator, mainly at the Defence Research and Development Organisation (DRDO) and Indian Space Research Organisation (ISRO) and was intimately involved in India's civilian space program and military missile development efforts."
		},
		"Sheryl Sandberg" : {
			description : "COO, Facebook",
			location : "Palo Alto, California, USA",
			company : "Facebook",
			big_description : "Sheryl Sandberg is an American technology executive, activist, and author. She is the Chief Operating Officer of Facebook. In June 2012, she was elected to the board of directors by the existing board members, becoming the first woman to serve on Facebook's board. Before she joined Facebook, Sandberg was Vice President of Global Online Sales and Operations at Google and was involved in launching Google's philanthropic arm Google.org. Before Google, Sandberg served as chief of staff for the United States Secretary of the Treasury.",
			quote : "We've got to get women to sit at the table."
		},
		"Mark Zuckerberg" : {
			description : "Founder and CEO, Facebook",
			location : "Palo Alto, California, USA",
			company : "Facebook",
			big_description: "Mark Elliot Zuckerberg (born May 14, 1984) is an American computer programmer and Internet entrepreneur. He is best known as one of five co-founders of the social networking website Facebook. Zuckerberg is the chairman and chief executive of Facebook, Inc.",
			quote: "Move fast and break things. Unless you are breaking stuff, you are not moving fast enough."
		},
		"Bill Gates" : {
			description : "Co-founder, Microsoft",
			location : "Washington, USA",
			company : "Microsoft",
			quote : "Success is a lousy teacher. It seduces smart people into thinking they can't lose.",
			big_description : "William Henry \"Bill\" Gates III (born October 28, 1955) is an American business magnate, philanthropist, investor, computer programmer, and inventor. In 1975, Gates co-founded Microsoft, that became the world's largest PC software company, with Paul Allen. During his career at Microsoft, Gates held the positions of chairman, CEO and chief software architect",
		},
		"Oprah Winfrey" : {
			description : "Host, The Oprah Winfrey Show",
			location : "USA",
			company : "The Oprah Winfrey Show",
			quote : "The biggest adventure you can ever take is to live the life of your dreams.",
			big_description : "Oprah Winfrey is an American media proprietor, talk show host, actress, producer, and philanthropist. She is best known for her talk show The Oprah Winfrey Show, which was the highest-rated program of its kind in history and was nationally syndicated from 1986 to 2011. Dubbed the \"Queen of All Media\", she has been ranked the richest African-American of the 20th century, the greatest black philanthropist in American history, and is currently North America's only black billionaire. Several assessments regard her as the most influential woman in the world."
		},
		"Elon Musk" : {
			description : "CEO, CTO at SpaceX, CTO at Tesla Motors, Chairman at SolarCity",
			location : "LA, California, USA",
			company : "SpaceX, Tesla, SolarCity",
			big_description : "Elon Reeve Musk is a South African-born Canadian-American business magnate,engineer, inventor and investor. He is the CEO and CTO of SpaceX, CEO and product architect of Tesla Motors, and chairman of SolarCity. He is the founder of SpaceX and a co-founder of Zip2, PayPal, and Tesla Motors.He has also envisioned a conceptual high-speed transportation system known as the Hyperloop and has proposed a VTOL supersonic jet aircraft with electric fan propulsion.",
			quote : "When something is important enough, you do it even if the odds are not in your favor."
		},
		"Ben Horowitz" : {
			description : "Co-founder, Andreessen Horowitz",
			location : "Silicon Valley, California, USA",
			company : "Andreessen Horowitz",
			big_description : "Ben Horowitz is an American businessman, investor, blogger, and author. He is a high technology entrepreneur and co-founder and general partner along with Marc Andreessen of the venture capital firm Andreessen Horowitz.",
			quote : "Often any decision, even the wrong decision, is better than no decision."
		},
		"Richard Branson" : {
			description : "Founder, Virgin Group",
			location : "British Virgin Islands",
			company : "Virgin Group",
			big_description : "Richard Branson is an English businessman and investor. He is best known as the founder of Virgin Group, which comprises more than 400 companies. At the age of sixteen his first business venture was a magazine called Student. In 1970, he set up a mail-order record business. In 1972, he opened a chain of record stores, Virgin Records, later known as Virgin Megastores. Branson's Virgin brand grew rapidly during the 1980s, as he set up Virgin Atlantic and expanded the Virgin Records music label.",
			quote : "You don't learn to walk by following rules. You learn by doing, and by falling over."
		},
		"NR Narayana Murthy" : {
			description : "Co-founder, Infosys",
			location : "India",
			company : "Infosys",
			big_description : "Narayana Murthy, is an Indian IT industrialist and the co-founder of Infosys, a multinational corporation providing business consulting, technology, engineering, and outsourcing services.Murthy has been listed among the 12 greatest entrepreneurs of our time by Fortune magazine. He has been described as Father of Indian IT sector by Time magazine due to his contribution to outsourcing in India. Murthy has also been honoured with the Padma Vibhushan and Padma Shri awards.",
			quote : "The real power of money is the power to give it away."
		}
	}

for (personName in data) {
	var bookTitles = data[personName];
	eachCount[personName] = bookTitles.length;
	 bookTitles.forEach(function(title){
	 	finalCount+=1;
	 });
}
var something = [];
var booksOfAPerson = {};
for (personName in data) {
	console.log('data is : '+data);
	console.log('personName is : '+personName);
	var personDescription = generalData[personName].description;
	var personDocument = new Person({
			name : personName,
			description : personDescription,
			location : generalData[personName].location,
			company : generalData[personName].company,
			big_description : generalData[personName].big_description,
			quote : generalData[personName].quote
	});
	Person.update({name : personName}, personDocument, {upsert : true}, function() {
		console.log('in callback');
	})
	// personDocument.save(function (err, personDocument){
	// 	if (err) return console.error(err);
	// 	console.log('saved person successfully!');
	// });

	var bookTitles = data[personName];
	data[personName] = [];
	bookTitles.forEach(function(title){
		(function(title, personName){
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
			    if (xhttp.readyState == 4 && xhttp.status == 200) {
			      parseString(xhttp.responseText, function (err, result) {
			      	console.log('Book name is : '+title);
	    			var jsonRequired = result.GoodreadsResponse.search[0].results[0].work[0];
	    			// response.send(jsonRequired);
	    			innerCount +=1;
	    			data[personName].push(jsonRequired);
	    			if(eachCount[personName] == data[personName].length) {
	    				var obj = {};
	    				obj["name"] = personName;
	    				obj["books"] = data[personName];
	    				something.push(obj);
	    			}
	    			if(innerCount == finalCount) {
			      		console.log('something is: ');
	    				console.log(something)
	    				//if all books for a person are found
	    				_.each(something, function(obj) { 
	    					var bookDocs = [];
			      			console.log('yaaaay');
			      			var personName = obj.name;
			      			console.log(personName);
			      			var booksArray = obj.books;
			      			_.each(booksArray, function(bookObject){
			      				var bookRating = bookObject.average_rating[0];
			      				var bookTitle = bookObject.best_book[0].title[0];
			      				var bookAuthor = bookObject.best_book[0].author[0].name;
			      				var bookPic = _.has(bookData[bookTitle], 'pic') ? bookData[bookTitle].pic : bookObject.best_book[0].image_url[0];
			      				console.log('book title is : '+bookTitle);
			      		
			      				var bookAmazonLink = bookData[bookTitle].affLink.amazon;
			      				var bookFlipkartLink = bookData[bookTitle].affLink.flipkart ? adjustForAffiliate(bookData[bookTitle].affLink.flipkart) : '';
			      				var bookDocument = new Book({ 
			      					title : bookTitle,
			      					author : bookAuthor,
			      					pic : bookPic,
			      					rating : bookRating,
			      					amazonLink : bookAmazonLink,
			      					flipkartLink : bookFlipkartLink
			      				});
			      				bookDocument.save(function (err, doc) {
								  if (err) return console.error(err);
								  console.log('saved successfully!');
								  bookDocs.push(doc);
								  console.log('bookDocs is: ');
									console.log(bookDocs);
									if(bookDocs.length == booksArray.length) {
										Person.update({name: personName}, {books : bookDocs},function(err, numberAffected, rawResponse){
						    				if (err) return console.error(err);
						    				console.log('Person updated!');
						    			});
									}
			      				});
							});
							

							
	    				});
	    			
	 
			      	}

				  

			    });
			}
		}
			xhttp.open("GET", "https://www.goodreads.com/search/index.xml?q="+title+"&key=UWhBKzBmPVptyDc7RM82g&search[%27title%27]&format=json", true);
			xhttp.send();
		})(title, personName);

 	})
}

function adjustForAffiliate(currentUrl) {
    var adjustedUrl = currentUrl;
    // Check for flipkart
    if(currentUrl.indexOf('flipkart.com')!=-1) {
        if(currentUrl.indexOf('/p/')==-1) {
            // Not product
        }
        else {
            adjustedUrl = adjustedUrl.replace('www','dl');
            adjustedUrl = adjustedUrl.replace('flipkart.com/','flipkart.com/dl/');
            adjustedUrl = removeParam('otracker',adjustedUrl);
            console.log(adjustedUrl);
            adjustedUrl = removeParam('ref',adjustedUrl);
            console.log(adjustedUrl);
            if(adjustedUrl.indexOf('?')!=-1) {
                adjustedUrl = adjustedUrl+'&affid=radhikabh1';
            }
            else if(adjustedUrl.indexOf('?')==-1) {
                adjustedUrl = adjustedUrl+'?affid=radhikabh1';
            }
            console.log(adjustedUrl);
        }
    } else if(currentUrl.indexOf('snapdeal.com')!=-1) {
        console.log('Snapdeal scene');
        if(adjustedUrl.indexOf('?')!=-1) {
            adjustedUrl = adjustedUrl.substr(0,adjustedUrl.indexOf('?'));
            adjustedUrl = adjustedUrl+'?utm_source=aff_prog&utm_campaign=afts&offer_id=17&aff_id=25590';
        } else if(adjustedUrl.indexOf('?')==-1) {
            adjustedUrl = adjustedUrl+'?utm_source=aff_prog&utm_campaign=afts&offer_id=17&aff_id=25590';
        }
    }
    return adjustedUrl;
}

function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

});//end of app.get

app.listen(3000,function(){
	console.log('listening on port 3000');
})

//change - end of power, muqaddimah
//fix lean startup title