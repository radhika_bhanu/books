var mongoose = require('mongoose');

var PersonSchema = new mongoose.Schema({
	name : String,
	description : String,
	location : String,
	company : String,
	big_description : String,
	quote : String,
	books : [{ type : mongoose.Schema.Types.ObjectId, ref : 'Book'}]
});

mongoose.model('Person', PersonSchema);
