var mongoose = require('mongoose');

var BookSchema = new mongoose.Schema({
	title : String,
	author : String,
	pic : String,
	rating : String,
	amazonLink : String,
	flipkartLink : String
});

mongoose.model('Book', BookSchema);