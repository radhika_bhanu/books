var bookData = {
	"The Player of Games (Culture, #2)" : {// no india
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1388426980i/12012._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1RvkRog",
			flipkart :"http://www.flipkart.com/iain-m-banks-consider-phlebas-player-games-use-weapons/p/itmdex4urfg4dubp?pid=9780356502090&ref=L%3A83036695816586115&srno=p_6&query=iain+banks&otracker=from-search"
		}
	},
	"The Ends of Power" : { // not india
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1411354727i/401465._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1Lnw6Kj",
			flipkart : "http://www.flipkart.com/end-power-english/p/itmdz6tf2zbmtryk?pid=9780465065691&ref=L%3A5896788914613929458&srno=p_1&query=the+ends+of+power&otracker=from-search"
		}
	},
	"The Innovator's Dilemma: The Revolutionary Book That Will Change the Way You Do Business" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1347289672i/9848159._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1RvjzcY",
			flipkart :"http://www.flipkart.com/innovator-s-dilemma-revolutionary-national-bestseller-changed-way-do-business-english/p/itme3u2kn7zgqbtf?pid=9780066620695&ref=L%3A4875851781565990262&srno=p_7&query=The+Innovator%27s+Dilemma%3A+The+Revolutionary+Book+That+Will+Change+the+Way+You+Do+Business&otracker=from-search"
		}
	},
	"Should We Eat Meat?: Evolution and Consequences of Modern Carnivory" : {
		affLink : {
			amazon : "http://www.amazon.com/gp/product/1118278720/ref=as_li_tl?ie=UTF8&camp=1789&creative=390957&creativeASIN=1118278720&linkCode=as2&tag=whatdotheyrea-20&linkId=C5IN2TYKIJC2MHWD",
			flipkart : "http://www.flipkart.com/should-eat-meat-evolution-consequences-modern-carnivory-english/p/itmdhz6ys5kfgakj?pid=9781118278727&ref=L%3A-5950077028056743935&srno=p_2&query=Should+We+Eat+Meat%3F%3A+Evolution+and+Consequences+of+Modern+Carnivory&otracker=from-search"
		}
	},
	"The Magic of Reality: How We Know What's Really True" : {
		affLink : {
			amazon : "http://amzn.to/1QZBdom",
			flipkart : "http://www.flipkart.com/magic-reality-know-what-s-really-true-english/p/itmdyhs4bgxyu4ts?pid=9781442341760&ref=L%3A8669594809565267249&srno=p_2&query=The+Magic+of+Reality%3A+How+We+Know+What%27s+Really+True&otracker=from-search"
		}
	},
	"On Immunity: An Inoculation" : {
		affLink : {
			amazon : "http://amzn.to/1PjHrB0",
			flipkart : "http://www.flipkart.com/on-immunity-an-inoculation/p/itme8xdfs7zz7qxp?pid=9781555976897&ref=L%3A-8124637173867263314&srno=p_1&query=On+Immunity%3A+An+Inoculation&otracker=from-search"
		}
	},
	"Hyperbole and a Half: Unfortunate Situations, Flawed Coping Mechanisms, Mayhem, and Other Things That Happened" : {
		affLink : {
			amazon : "http://amzn.to/1OnkLiL",
			flipkart : "http://www.flipkart.com/hyperbole-half-unfortunate-situations-flawed-coping-mechanisms-mayhem-other-things-happened-english/p/itme4rz649kqyy47?pid=9780224095372&ref=L%3A8895405855863685221&srno=p_1&query=Hyperbole+and+a+Half%3A+Unfortunate+Situations%2C+Flawed+Coping+Mechanisms%2C+Mayhem%2C+and+Other+Things+That+Happened&otracker=from-search"
		}
	},
	"How to Lie with Statistics" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1384474531i/18805905._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1PjHwET",
			flipkart : "http://www.flipkart.com/lie-statistics-english-reissue-4th/p/itme9hf5thamyjxe?pid=9780393310726&ref=L%3A8242699976335772473&srno=p_1&query=How+to+Lie+with+Statistics&otracker=from-search"
		}
	},
	"Only the Paranoid Survive " : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1396021991i/66864._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1Lntdt2",
			flipkart : "http://www.flipkart.com/only-paranoid-survive-english/p/itme8ussu38ghvwm?pid=9781861975133&ref=L%3A887925229179195052&srno=p_1&query=Only+the+Paranoid+Survive+&otracker=from-search"
		}
	},
	"xkcd: volume 0" : {
		affLink : {
			amazon : "http://amzn.to/1RvjFRZ",
			flipkart : "http://www.flipkart.com/xkcd-english/p/itmczzjxtsxgpqyn?pid=9780615314464&icmpid=reco_pp_same_book_book_1&ppid=9785458921190"
		}
	},
	"The Black Jacobins: Toussaint L'Ouverture and the San Domingo Revolution" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1349074537i/113165._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1PjHLzG",
			flipkart : "http://www.flipkart.com/black-jacobins-toussaint-l-ouverture-san-domingo-revolution-english/p/itme4bufwzkjp3wy?pid=9780679724674&ref=L%3A-7153230391165694848&srno=p_2&query=The+Black+Jacobins%3A+Toussaint+L%27Ouverture+and+the+San+Domingo+Revolution&otracker=from-search"
		}
	},
	"The Power Of Focus" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1348442147i/11056574._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1LntMmy",
			flipkart : "http://www.flipkart.com/power-focus-english/p/itmdyspc7u45gnst?pid=9780091948221&ref=L%3A-6770229431573985492&srno=p_1&query=the+power+of+focus&otracker=from-search"
		}
	},
	"Howard Hughes: The Untold Story" : {
		affLink : {
			amazon : "http://amzn.to/1OnlobP",
			flipkart : "http://www.flipkart.com/howard-hughes-untold-story-english/p/itme9hfs7qtsungt?pid=9780306813924&ref=L%3A4407346068460158994&srno=p_1&query=Howard+Hughes%3A+The+Untold+Story&otracker=from-search"
		}
	},
	"Technological Revolutions and Financial Capital: The Dynamics of Bubbles and Golden Ages" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1302073297i/9894874._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1LntZ9o",
			flipkart : "http://www.flipkart.com/technological-revolutions-financial-capital-dynamics-bubbles-golden-ages-english/p/itmdyh8agb6gcfmw?pid=9781843763314&ref=L%3A-7219659477854697491&srno=p_1&query=Technological+Revolutions+and+Financial+Capital%3A+The+Dynamics+of+Bubbles+and+Golden+Ages&otracker=from-search"
		}
	},
	"What If?: Serious Scientific Answers to Absurd Hypothetical Questions" : {
		affLink : {
			amazon : "http://amzn.to/1Rvk7zA",
			flipkart : "http://www.flipkart.com/if-serious-scientific-answers-absurd-hypothetical-questions/p/itme9y5ajxkhpdje?pid=9781848549586&ref=L%3A-5176779643976726767&srno=p_1&query=What+If%3F%3A+Serious+Scientific+Answers+to+Absurd+Hypothetical+Questions&otracker=from-search"
		}
	},
	"Einstein: His Life and Universe" : {
		affLink : {
			amazon : "http://amzn.to/1PjI2ml",
			flipkart : "http://www.flipkart.com/einstein-his-life-universe/p/itme8nndf5kzntgp?pid=DGBDFBZJHWCXFHJW&ref=L%3A-2281524841627433653&srno=p_1&query=Einstein%3A+His+Life+and+Universe&otracker=from-search"
		}
	},
	"My Years with General Motors" : {
		pic : "https://i.gr-assets.com/images/S/photo.goodreads.com/books/1348442995i/16042934._UY200_.jpg",
		affLink : {
			amazon : "http://amzn.to/1Rvko5J",
			flipkart : "http://www.flipkart.com/my-years-general-motors-english/p/itmczz46jzjtd75g?pid=9780385042352&ref=L%3A1139218216003518572&srno=p_1&query=My+Years+with+General+Motors&otracker=from-search"
		}
	},
	"The Hard Thing About Hard Things: Building a Business When There Are No Easy Answers" : {
		affLink : {
			amazon : "http://amzn.to/1Rvkw4X",
			flipkart : "http://www.flipkart.com/hard-thing-things-building-business-there-no-easy-answers/p/itme37hpzryjzfbb?pid=DGBDTJR3MXT4A4GC&ref=L%3A-8645542411795805676&srno=p_1&query=The+Hard+Thing+About+Hard+Things%3A+Building+a+Business+When+There+Are+No+Easy+Answers&otracker=from-search"
		}
	},
	"Benjamin Franklin: An American Life" : {
		affLink : {
			amazon : "http://amzn.to/1RvkB8W",
			flipkart : "http://www.flipkart.com/benjamin-franklin-american-life-english/p/itme8n859x3jbxcc?pid=9780684807614&ref=L%3A-8343035215280847503&srno=p_1&query=Benjamin+Franklin%3A+An+American+Life&otracker=from-search"
		}
	},
	"Ignition (Ignition Trilogy, #1)" : {
		affLink : {
			amazon : "http://amzn.to/1Lnv7tu"
		}
	},
	"Catherine the Great: Portrait of a Woman" : {
		affLink : {
			amazon : "http://amzn.to/1Lnvd4t",
			flipkart : "http://www.flipkart.com/catherine-great-portrait-woman-english/p/itme8n84tayuatgg?pid=9780345408778&ref=L%3A4731827918300856713&srno=p_1&query=Catherine+the+Great%3A+Portrait+of+a+Woman&otracker=from-search"
		}
	},
	"The Better Angels of Our Nature: Why Violence Has Declined" : {
		affLink : {
			amazon : "http://amzn.to/1Onnw3l",
			flipkart : "http://www.flipkart.com/better-angels-our-nature-violence-has-declined-english/p/itme576kdtwgwnzu?pid=9780143122012&ref=L%3A4002474946583675222&srno=p_1&query=The+Better+Angels+of+Our+Nature%3A+Why+Violence+Has+Declined&otracker=from-search"
		}
	},
	"Sapiens: A Brief History of Humankind" : {
		affLink : {
			amazon : "http://amzn.to/1OnnCrN",
			flipkart : "http://www.flipkart.com/sapiens-brief-history-humankind/p/itme9y3nh4f4aq8b?pid=9781846558245&ref=L%3A-1076715148720913953&srno=p_1&query=Sapiens%3A+A+Brief+History+of+Humankind&otracker=from-search"
		}
	},
	"Creativity, Inc.: Overcoming the Unseen Forces That Stand in the Way of True Inspiration" : {
		affLink : {
			amazon : "http://amzn.to/1OnnGYt",
			flipkart : "http://www.flipkart.com/creativity-inc-overcoming-unseen-forces-stand-way-true-inspiration-english/p/itmdzar5s2fhmv3e?pid=9780593070109&ref=L%3A-7396808933550698985&srno=p_1&query=Creativity%2C+Inc.%3A+Overcoming+the+Unseen+Forces+That+Stand+in+the+Way+of+True+Inspiration&otracker=from-search"
		}
	},
	"Muqaddimah Aqidah Muslimin" : {
		affLink : {
			amazon : "",
			flipkart : "http://www.flipkart.com/muqaddimah-introduction-history-english/p/itme2wknttfx4ytv?pid=9780691166285&ref=L%3A5760362653088366881&srno=p_1&query=muqqadimah&otracker=from-search"
		}
	}, 
	"The New Jim Crow: Mass Incarceration in the Age of Colorblindness" : {
		affLink : {
			amazon : "http://amzn.to/1LnvUuF",
			flipkart : "http://www.flipkart.com/new-jim-crow-english/p/itmdyk7u6nwfrsne?pid=9781595581037&ref=L%3A328462671577498155&srno=p_1&query=The+New+Jim+Crow&otracker=from-search	"
		}
	}
};