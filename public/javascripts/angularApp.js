var app = angular.module('books',['ui.router']);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
	.state('home', {
		url : '/home',
		templateUrl : '/home.html',
		controller : 'MainCtrl',
		resolve: {
		    postPromise: ['data', function(data){
		      //console.log(data.getPeopleData());
		      return data.getPeopleData();
		    }]
		}
	})
	.state('books', {
		url : '/books/{id}',
		templateUrl : '/books.html',
		controller : 'BooksCtrl',
		resolve : {
			booksOfAPerson: ['$stateParams', 'data', function($stateParams, data) {
	      	return data.getBooksDataForAPerson($stateParams.id);
	    	}]
		}
	})

	$urlRouterProvider.otherwise('home');
}])

app.controller('MainCtrl', ['$scope', 'data', '$timeout' , function($scope, data, $timeout){
	 $scope.spinner = true;
	$scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if (toState.resolve) {
    	 $timeout(function(){
         	$scope.spinner = true;
         });
    	// console.log('showing main');
            }
});
$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
    if (toState.resolve) {
    	 $timeout(function(){
         	$scope.spinner = false;
         });
        // console.log('hiding main');
        	$scope.people = data.people;
	$scope.books = data.books;
    }
});

}]);

app.controller('BooksCtrl', ['$scope', 'data', '$timeout','$stateParams', 'booksOfAPerson', function($scope, data, $timeout, $stateParams, booksOfAPerson){
		$scope.spinner = true;
		$scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if (toState.resolve) {
         $timeout(function(){
         	$scope.spinner = true;
         });
        // console.log('showiung books');
            }
});
$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
    if (toState.resolve) {
    	 $timeout(function(){
         	$scope.spinner = false;
         });
        
        // console.log('hiding books');
    }
});
	$scope.books = booksOfAPerson.books;

}]);

app.factory('data', ['$http', function($http){
	var o = {
		people : [],
	};
	o.getPeopleData = function() {
		 return $http.get('/people').success(function(data){
      			angular.copy(data, o.people);
      			// console.log(data);
     	  });
	}
	o.getBooksDataForAPerson = function(id) {
		  return $http.get('/books/' + id).then(function(res){
		    return res.data;
		  });
	}
	return o;
}])

app.run(function($rootScope){
	$rootScope.$on('$stateChangeSuccess', function() {
   		document.body.scrollTop = document.documentElement.scrollTop = 0;
	});
})